package lab2.task1;

import com.thoughtworks.xstream.XStream;

import java.io.FileReader;

public class ParseXML {
    public static void main(String[] args) {
        XStream xstream = new XStream();
        xstream.alias("Car", Car.class);
        xstream.alias("Engine", Engine.class);

        Car car = null;
        StringBuilder sb = new StringBuilder();
        try {
            FileReader inputStream = new FileReader("car.xml");

            int c;
            while ((c = inputStream.read()) != -1) {
                sb.append((char)c);
            }
            String xml = sb.toString();

            Car newAcura = (Car) xstream.fromXML(xml);

            System.out.println(newAcura.getMake() + " " + newAcura.getModel());
            System.out.println(newAcura.getEngine());
        } catch (Exception e) {
            //TODO
        }
    }
}
