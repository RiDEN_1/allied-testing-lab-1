package lab2.task1;

import com.thoughtworks.xstream.XStream;

import java.io.FileOutputStream;
import java.io.PrintWriter;

public class CreateXML {
    public static void main(String[] args) {
        XStream xstream = new XStream();
        xstream.alias("car", Car.class);
        xstream.alias("engine", Engine.class);

        Car acura = new Car("Acura", "RSX");
        acura.setEngine(new Engine("K20A3", 2000, 170));

        String xml = xstream.toXML(acura);
        try {
            FileOutputStream fileOut = new FileOutputStream("car.xml");
            PrintWriter outputStream = new PrintWriter(fileOut);
            outputStream.print(xml);
            outputStream.flush();
            outputStream.close();
            fileOut.close();
        } catch (Exception e) {
            //TODO
        }

    }
}
