package lab2.task1;

public class Engine {
    private String model;
    private double volume;
    private double power;

    public Engine() {
    }

    public Engine(String model, double volume, double power) {
        this.model = model;
        this.volume = volume;
        this.power = power;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public double getVolume() {
        return volume;
    }

    public void setVolume(double volume) {
        this.volume = volume;
    }

    public double getPower() {
        return power;
    }

    public void setPower(double power) {
        this.power = power;
    }

    @Override
    public String toString() {
        return ("Engine model:" + model + "\nVolume:" + volume + "\nPower:" + power);
    }
}
